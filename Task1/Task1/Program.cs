﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using VkNet;
using VkNet.Enums.Filters;
using System.Linq;
using System.IO;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Авторизация и получение токена
            Console.WriteLine("Введите логин");
            string login = Console.ReadLine();
            Console.WriteLine("Введите пароль");
            string pas = Console.ReadLine();
            VkApi vk = new VkApi();
            var param = new VkNet.Model.ApiAuthParams()
            {
                ApplicationId = 7479194,
                Login = login,
                Password = pas
            };
            vk.Authorize(param);
            //Получение списка друзей
            var friendsList = vk.Friends.Get(new VkNet.Model.RequestParams.FriendsGetParams());
            //Получение списка моих групп
            var myGroups = vk.Groups.Get(new VkNet.Model.RequestParams.GroupsGetParams()).Select(x=>x.Id);
            //Запись в файл task1.txt в таблицу вида UserId друга табуляция количество общих групп
            var sw = new StreamWriter("task1.txt");
            var t = 0;
            foreach (var v in friendsList)
            {
                t = 0;
                try
                {
                    sw.Write(v.Id + "\t");
                    //Получение списка групп друга
                    var res = vk.Groups.Get(new VkNet.Model.RequestParams.GroupsGetParams() { UserId = v.Id }).Select(x=>x.Id);
                    foreach (var i in res)
                    {
                        //Есили нашли совпадающую у меня и друга группу инкрементируем счётчик 
                        if (myGroups.Contains(i)) t++;
                    }
                    sw.WriteLine(t);
                }
                catch { sw.WriteLine(t); }
            }
            sw.Dispose();
        }
    }
}
